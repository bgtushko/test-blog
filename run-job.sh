curl --request POST --header "PRIVATE-TOKEN: $token" \
    --form "variables[][key]=CI_REGISTRY_IMAGE" \
    --form "variables[][value]=$CI_REGISTRY_IMAGE" \
    --form "variables[][key]=CI_COMMIT_REF_NAME" \
    --form "variables[][value]=$CI_COMMIT_REF_NAME" \
    --form "variables[][key]=PORT" \
    --form "variables[][value]=$PORT" \
    --form "variables[][key]=NGINX" \
    --form "variables[][value]=$NGINX" \
    --form "variables[][key]=APP" \
    --form "variables[][value]=$APP" \
    "https://gitlab.com/api/v4/projects/31154592/pipeline?ref=main" | jq .
JOB_ID=$(curl -k --location --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/31154592/jobs" | jq '[.[] | select(.name == "run")][0] | .id')
curl -k --request POST --header "PRIVATE-TOKEN: $token" "https://gitlab.com/api/v4/projects/31154592/jobs/$JOB_ID/play" 
